# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as homeassistant_docker with context %}

{%- set mountpoint = homeassistant_docker.docker.mountpoint %}

homeassistant-docker-mountdir-managed:
  file.directory:
    - name: {{ mountpoint }}/docker/homeassistant
    - makedirs: True

homeassistant-docker-broker-image-present:
  docker_image.present:
    - name: {{ homeassistant_docker.container.image }}
    - tag: {{ homeassistant_docker.container.tag }}
    - force: True

homeassistant-docker-container-running:
  docker_container.running:
    - name: {{ homeassistant_docker.container.name }}
    - image: {{ homeassistant_docker.container.image }}:{{ homeassistant_docker.container.tag }}
    - restart: unless-stopped
    - binds:
      - {{ mountpoint }}/docker/homeassistant/config:/config
    - network_mode: host
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
    - labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.homeassistant-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.homeassistant-web.middlewares=homeassistant-redirect-websecure"
      - "traefik.http.routers.homeassistant-web.rule=Host(`{{ homeassistant_docker.container.url }}`)"
      - "traefik.http.routers.homeassistant-web.entrypoints=web"
      - "traefik.http.routers.homeassistant-websecure.rule=Host(`{{ homeassistant_docker.container.url }}`)"
      - "traefik.http.routers.homeassistant-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.homeassistant-websecure.tls=true"
      - "traefik.http.routers.homeassistant-websecure.entrypoints=websecure"
      - "traefik.http.services.homeassistant.loadBalancer.server.port=8123"
