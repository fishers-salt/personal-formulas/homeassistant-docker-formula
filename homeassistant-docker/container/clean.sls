# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as homeassistant_docker with context %}

{%- set name = homeassistant_docker.container.name %}
{%- set image = homeassistant_docker.container.image %}

homeassistant-docker-container-clean-container-absent:
  docker_container.absent:
    - name: {{ name }}
    - force: True

homeassistant-docker-docker-clean-image-absent:
  docker_image.absent:
    - name: {{ image }}
    - require:
      - homeassistant-docker-container-clean-container-absent
