# frozen_string_literal: true

describe docker_container(name: 'homeassistant') do
  it { should exist }
  it { should be_running }
  its('image') { should eq 'lscr.io/linuxserver/homeassistant:latest' }
  it { should have_volume('/config', '/srv/docker/homeassistant/config') }
end
